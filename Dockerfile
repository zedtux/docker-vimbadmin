FROM corbinu/docker-nginx-php
MAINTAINER Guillaume Hain zedtux@zedroot.org

ENV VIMBADMIN_VERSION 3.0.12
ENV APPLICATION_PATH  /www
# NB: 2015-02-02 - due to #124, the database currently needs to be called vimbadmin.
ENV MYSQL_DB_NAME vimbadmin
ENV MYSQL_DB_USERNAME vimbadmin

# 1. Install mysql command to create MySQL database and user in case is missing
# 2. Git clone the project
# 3. Checkout specific version
# 4. Run composer to install dependencies
# 5. Force permissions on var/ application folder as requested by documentation
RUN apt-get update && \
    apt-get install -y mysql-client-core-5.6 && \
    git clone https://github.com/opensolutions/ViMbAdmin.git $APPLICATION_PATH && \
    cd $APPLICATION_PATH/ && \
    git checkout $VIMBADMIN_VERSION && \
    composer install --dev && \
    chown -R www-data: $APPLICATION_PATH/var && \
    cp $APPLICATION_PATH/public/.htaccess.dist $APPLICATION_PATH/public/.htaccess

# Forward request and error logs to docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log

# Add configuration file (will be updated by the run script)
ADD etc/application.ini $APPLICATION_PATH/application/configs/application.ini
# Add the run script
ADD bin/run /usr/local/bin/run

WORKDIR /www
EXPOSE 80

CMD ["/usr/local/bin/run"]
