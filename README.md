# ViMbAdmin docker image

This repository contains the work in progress of a docker image of ViMbAdmin.

The image is created based on a PHP and Nginx docker image and install a specific
version of ViMbAdmin (cloned from Github).

# TODO

 * Auto generate the `securitysalt`, `resources.auth.oss.rememberme.salt` and
 `defaults.mailbox.password_salt`
 * Find a way to provide the `securitysalt` to the user in order to create the
 initial account
 * Configure `resources.mailer.smtphost`

# Build the image

As the image is not finished, I didn't pushed it to docker.com so you need to
build it yourself.

    $ git clone https://github.com/zedtux/docker-vimbadmin
    $ cd docker-vimbadmin/
    sudo docker build -t <username>/vimbadmin .

# Run the image

First start a MySQL image

    sudo docker run --name mysql -e MYSQL_ROOT_PASSWORD=test -d mysql

Then start the VuMbAdmin image

    sudo docker run --name vimbadmin \
      --link mysql:mysql \
      -e MYSQL_ROOT_PASSWORD=test \
      -e MYSQL_DB_PASSWORD=zedtux \
      zedtux/vimbadmin
